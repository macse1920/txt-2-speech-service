from io import BytesIO
from pathlib import Path

from flask import Flask, request, Response, send_file
import py_eureka_client.eureka_client as eureka_client
from gtts import gTTS
import os
import uuid
import logging
import utils

app = Flask(__name__)
log = logging
log.basicConfig(level=logging.DEBUG)
eureka_client.init_registry_client(eureka_server=utils.EUREKA_SERVER,
                                   app_name=utils.APP_NAME,
                                   instance_port=utils.INSTANCE_PORT)


@app.route(utils.SAVE_AUDIO_FILE_PATH, methods=['POST'])
def save_audio_file():
    try:

        req_data = request.get_json()

        content = req_data['content']

        if not content:
            return Response(utils.EMPTY_CONTENT_MESSAGE, status=utils.BAD_REQUEST_STATUS_CODE,
                            mimetype=utils.JSON_MIME_TYPE)

        tts = gTTS(content)
        file_name = str(uuid.uuid1()) + utils.OUTPUT_FORMAT
        tts.save(file_name)

        return send_file(file_name, attachment_filename='audio.mp3', as_attachment=True)
    except Exception as e:
        return Response(str(e), status=utils.BAD_REQUEST_STATUS_CODE, mimetype=utils.JSON_MIME_TYPE)


if __name__ == '__main__':
    app.run()
