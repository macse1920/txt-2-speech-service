BAD_REQUEST_STATUS_CODE = 400
OUTPUT_FORMAT = ".mp3"
JSON_MIME_TYPE = "application/json"

# eureka config
EUREKA_SERVER = "http://localhost:8761/eureka"
APP_NAME = "txt-2-speech-service"
INSTANCE_PORT = 5000


# save_audio_file() utils
EMPTY_CONTENT_MESSAGE = "Content should not be empty!"
SAVE_AUDIO_FILE_PATH = "/convert"
